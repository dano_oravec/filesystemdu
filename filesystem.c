#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <fcgimisc.h>
#include <stdlib.h>

#include "filesystem.h"
#include "util.h"

/* file_t info items */
#define FILE_T_OFFSET 0

typedef struct {
	char name[MAX_FILENAME];
	uint32_t size;
} zero_sector_t;

// %%%%%%%%%%%%%%%%%%%%%% VSEOBECNE POMOCNE FUNKCIE %%%%%%%%%%%%%%%%%%%%%%%%%%

uint8_t filenames_equal(const char* a, const char* b) {
    for(uint8_t i = 0; i < MAX_FILENAME; i++) {
        if(a[i] != b[i])
            return 0;
    }
    return 1;
}
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// $$$$$$$$$$$$$$$$$$$$$$ PRACA S FILE DESCRIPTORMI / HANDLES $$$$$$$$$$$$$$$$$$$$$$$

void handle_set_inode(file_t* fd, uint32_t inode_num) {
    fd->info[0] = inode_num;
}

void handle_set_position(file_t* fd, uint32_t position) {
    fd->info[1] = position;
}

void handle_set_curr_item_sector(file_t* fd, uint32_t curr_item_sector) {
    fd->info[2] = curr_item_sector;
}

void handle_set_curr_item_slot(file_t* fd, uint8_t curr_item_sector) {
    fd->info[3] = (uint32_t)curr_item_sector;
}

uint32_t handle_get_inode_num(file_t* fd) {
    return fd->info[0];
}

uint32_t handle_get_position(file_t* fd) {
    return fd->info[1];
}

uint32_t handle_get_curr_item_sector(file_t* fd) {
    return fd->info[2];
}

uint8_t handle_get_curr_item_slot(file_t* fd) {
    return (uint8_t)fd->info[3]; // Ak nam nikto nebude zasahovat do handlu, tak ten cast je bezpecny
}

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

// ---------------------------------------------------------------------

// Pri formatovani vytvorime priestor pre hdd_size() / BYTES_PER_INODE inodov
#define BYTES_PER_INODE (128 * 3)

uint32_t num_sectors() {
    assert(hdd_size() % SECTOR_SIZE == 0);
    return (uint32_t)(hdd_size() / SECTOR_SIZE);
}

/**
 * Pocet sektorov, ktore zabera bitmapa sektorov
 */
uint32_t sector_bm_len() {
    return num_sectors() / (8 * SECTOR_SIZE) + 1; // Vratime radsej nieco trochu vacsie, ako treba, kvoli zaokruhlovaniu
}

/**
 * Pocet inodov
 */
uint32_t num_inodes() {
    return (uint32_t)(hdd_size() / BYTES_PER_INODE);
}

/**
 * Pocet sektorov, ktore zabera bitmapa inodov
 */
uint32_t inode_bm_len() {
    return num_inodes() / (8 * SECTOR_SIZE) + 1;
}

/**
 * Cislo prveho sektoru, ktory zabera bitmapa inodov
 */
uint32_t inode_bm_first_sector() {
    return sector_bm_len();
}

/**
 * Cislo sektoru, na ktorom zacina prvy inode. Inode ma velkost 1 sektor, takze druhy bude na first_inode_sector() + 1..
 */
uint32_t first_inode_sector() {
    return sector_bm_len() + inode_bm_len(); // Sektory sa cisluju od 0
}

// ********** PRACA S BITMAPOU BLOKOV ***********

#define USED 1
#define UNUSED 0

#define NO_FREE_SECTOR 1589934591

/**
 * Oznaci sektor s cislom sector_num ako pouzity, ak mark_as_used == USED, inac za nepouzity
 */
void mark_sector(uint32_t sector_num, uint8_t mark_as_used) {
    uint32_t bm_sector = sector_num / (SECTOR_SIZE * 8);
    uint32_t sector_byte = (sector_num / 8) % SECTOR_SIZE;
    uint8_t byte_bit = (uint8_t)(sector_num % 8);
    uint8_t buffer[SECTOR_SIZE];
    hdd_read(bm_sector, buffer);
    if(mark_as_used == USED) {
        buffer[sector_byte] |= (1 << (7-byte_bit)); // 7-, lebo chceme mat bity zlava doprava cez vsetky bajty
    }
    else {
        buffer[sector_byte] &= ~(1 << (7-byte_bit));
    }
    hdd_write(bm_sector, buffer);
}

void make_sector_zero(uint32_t sector_num) {
    uint8_t sector[SECTOR_SIZE] = {0};
    hdd_write(sector_num, sector);
}

/**
 * Vrati cislo najlavejsieho volneho sektora
 */
uint32_t get_free_sector_num() {
    uint8_t buffer[SECTOR_SIZE];
    uint32_t max_sectors = num_sectors();
    uint32_t passed = 0; // Budeme si pocitat kolko bitov sme presli, aby sme vedeli kedy sme uz presli vsetky z bitmapy sektorov
    for(uint32_t i = 0; i < sector_bm_len(); i++) {
        hdd_read(i, buffer);
        for(uint32_t j = 0; j < SECTOR_SIZE; j++) {
            if(passed >= max_sectors) {
                return NO_FREE_SECTOR; // Uz sme presli vsetky a ziaden sme nenasli
            }
            uint8_t byte = buffer[j];
            if(byte != 255) {
                for(int bit = 0; bit < 8; bit++) {
                    if(passed >= max_sectors) {
                        return NO_FREE_SECTOR; // Uz sme presli vsetky a ziaden sme nenasli
                    }
                    if(!(byte & (1 << (7-bit)))) {
                        uint32_t sector_num = i * SECTOR_SIZE * 8 + j * 8 + bit;
                        make_sector_zero(sector_num); // Pred pouzivanim ho znulujeme
                        return sector_num;
                    }
                    passed++;
                }
            }
            else {
                passed += 8;
            }
        }
    }
    return NO_FREE_SECTOR;
}


// **********************************************

// ^^^^^^^^^^ PRACA S BITMAPOU INODOV ^^^^^^^^^^^

#define NO_FREE_INODE 1589934591

/**
 * Oznaci inode s cislom inode_num ako pouzity, ak mark_as_used == USED, inac za nepouzity
 */
void mark_inode(uint32_t inode_num, uint8_t mark_as_used) {
    uint32_t bm_sector = inode_bm_first_sector() + (inode_num / (SECTOR_SIZE * 8));
    uint32_t sector_byte = (inode_num / 8) % SECTOR_SIZE;
    uint8_t byte_bit = (uint8_t)(inode_num % 8);
    uint8_t buffer[SECTOR_SIZE];
    hdd_read(bm_sector, buffer);
    // Tie 7- su tam, lebo chceme mat bity zlava doprava
    if(mark_as_used == USED) {
        buffer[sector_byte] |= (1 << (7-byte_bit));
    }
    else {
        buffer[sector_byte] &= ~(1 << (7-byte_bit));
    }
    hdd_write(bm_sector, buffer);
}

/**
 * Z bitmapy inodov zisti, ci je inode cislo inode_num pouzivane nejakym suborom
 */
uint32_t inode_used(uint32_t inode_num) {
    uint32_t bm_sector = inode_bm_first_sector() + (inode_num / (SECTOR_SIZE * 8));
    uint32_t sector_byte = (inode_num / 8) % SECTOR_SIZE;
    uint8_t byte_bit = (uint8_t)(inode_num % 8);
    uint8_t buffer[SECTOR_SIZE];
    hdd_read(bm_sector, buffer);
    if(buffer[sector_byte] & (1 << (7-byte_bit))) // Ta 7- je tam, lebo chceme mat bity zlava doprava
        return 1;
    return 0;
}

/**
 * Vrati cislo najlavejsieho volneho inodu
 */
uint32_t get_free_inode_num() {
    uint8_t buffer[SECTOR_SIZE];
    uint32_t max_num_inodes = num_inodes();
    uint32_t passed = 0; // Budeme si pocitat prejdene bity, aby sme vedeli, kedy uz sme presli vsetky inody
    // Prejdeme cez vsetky bloky bitmapy inodov
    for(uint32_t i = 0; i < inode_bm_len(); i++) {
        hdd_read(inode_bm_first_sector() + i, buffer);
        // Prejdeme cez vsetky bajty v akutalnom bloku
        for(uint32_t j = 0; j < SECTOR_SIZE; j++) {
            if(passed >= max_num_inodes) {
                return NO_FREE_INODE; // Uz sme skontrolovali vsetky a nic sme nenasli
            }
            uint8_t byte = buffer[j];
            // Ak to nie su same 1, tak najdeme najlavejsi nulovy bit (to je volny inode)
            if(byte != 255) {
                for(int bit = 0; bit < 8; bit++) {
                    if(passed >= max_num_inodes)
                        return NO_FREE_INODE; // Uz sme skontrolovali vsetky a nic sme nenasli
                    if(!(byte & (1 << (7-bit)))) {
                        return i * SECTOR_SIZE * 8 + j * 8 + bit;
                    }
                    passed++;
                }
            }
            else {
                passed += 8;
            }
        }
    }
    return NO_FREE_INODE;
}

// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

// ========== PRACA S INODE ============

#define NOTHING 16777215

#define SUCCESS 1

#define NUM_SLOTS ((uint32_t)6)
// is_occupied, name, inode_num, nxt_item_block, nxt_item_slot = 1 + MAX_FILENAME + 3 + 3 + 1 = 20 bytes
#define SLOT_SIZE ((uint32_t)20)

void write_inode(uint32_t inode_num, uint8_t* inode) {
    hdd_write(first_inode_sector() + inode_num, inode);
}

void read_inode(uint32_t inode_num, uint8_t* inode) {
    hdd_read(first_inode_sector() + inode_num, inode);
}


/**
 * Typ suboru, ktory je reprezentovany inodom inode.
 * 0 = file
 * 1 = directory
 * 2 = hardlink
 * 3 = symlink
 */
uint8_t inode_get_type(const uint8_t* inode) {
    return inode[0] >> 6;
}

uint8_t inode_get_type_by_num(uint32_t inode_num) {
    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);
    return inode_get_type(inode);
}

/**
 * Pocet hardlinkov ukazujucich na inode inode
 */
uint32_t inode_get_num_hardlinks(const uint8_t* inode) {
    uint32_t ans = inode[1];
    for(uint32_t i = 0; i < 6; i++) {
        if(inode[0] & (1 << i)) {
            ans += 1 << (i + 8);
        }
    }
    return ans;
}

/**
 * Precita num_bytes-bytove cislo z bufferu buffer, ktore v zacina na byte cislo from_byte (cislovane od 0)
 */
uint32_t read_number_from_buffer(const uint8_t* buffer, uint32_t num_bytes, uint32_t from_byte) {
    uint32_t ans = 0;
    for(uint32_t i = 0, shift_bits = (num_bytes - 1) * 8; i < num_bytes; i++, shift_bits -= 8) {
        ans += ((uint32_t)(buffer[from_byte + i])) << (shift_bits);
    }
    return ans;
}

void write_number_to_buffer(uint8_t* buffer, uint32_t num_bytes, uint32_t from_byte, uint32_t number) {
    for(uint32_t i = 0, shift_bits = (num_bytes - 1) * 8; i < num_bytes; i++, shift_bits -= 8) {
        buffer[from_byte + i] = (uint8_t)(number >> shift_bits);
    }
}

/**
 * Vrati velkost suboru reprezentovaneho inodom inode v bytoch
 */
uint32_t inode_get_size(uint8_t* inode) {
    return read_number_from_buffer(inode, 4, 2);
}

/**
 * Vrati cisl idx-teho dietata indirect blocku cislo indirect_num
 */
int get_child_num(uint32_t indirect_num, uint32_t idx) {
    uint8_t indirect_block[SECTOR_SIZE];
    hdd_read(indirect_num, indirect_block);

    uint32_t child_num = read_number_from_buffer(indirect_block, 3, 3 * idx);
    // Ak este takyto child sector neexistuje (0 berieme ako neexistujuci pointer), tak ho vytvorime
    if(child_num == 0) {
        uint32_t free_sector_num = get_free_sector_num();
        if(free_sector_num == NO_FREE_SECTOR) {
            return FAIL; // Uz nie je volny sektor, ktory by sme mohli pouzit
        }
        mark_sector(free_sector_num, USED);
        write_number_to_buffer(indirect_block, 3, 3 * idx, free_sector_num);
        hdd_write(indirect_num, indirect_block); // Zapiseme to s updatnutym child blockom
    }

    child_num = read_number_from_buffer(indirect_block, 3, 3 * idx); // updatneme to pre pripad, ze by sa to v tom ife vyssie zmenilo
    return child_num;
}

/**
 * Ako write_tree, ale na citanie
 */
int read_tree(uint32_t indirect_block_num, uint32_t level, uint32_t dest_idx, uint8_t* resulting_block) {
    uint32_t level_sizes[6] = {1, 42, 42*42, 42*42*42, 42*42*42*42, 42*42*42*42*42};

    // Ak sme uz v liste, tak ho nacitame a koncime
    if(level == 0) {
        hdd_read(indirect_block_num, resulting_block);
        return OK;
    }

    uint32_t to_skip = dest_idx / level_sizes[level - 1];
    int child_block_num = get_child_num(indirect_block_num, to_skip);
    if(child_block_num == FAIL)
        return FAIL;
    int status = read_tree((uint32_t)child_block_num, level - 1, dest_idx - to_skip * level_sizes[level - 1], resulting_block);
    return status;
}

/**
 * n-ty sektor suboru reprezentovaneho inodom inode ulozi do resulting_sector, sektory su cislovane od 0
 * Miesto, kde je ulozeny pointer na dany sektor sa ulozi do pointer_sector a pointer_location_in_sector
 */
int inode_read_nth_sector(uint8_t* inode, uint32_t n, uint8_t* resulting_sector) {
    uint32_t file_size = inode_get_size(inode);
    uint32_t num_file_sectors = (file_size / SECTOR_SIZE) + (file_size % SECTOR_SIZE > 0);
    assert(num_file_sectors > n); // Ci ma file naozaj ten sektor ktory chceme precitat

    uint32_t prefix[6] = {
            35,
            35 + 42,
            35 + 42 + 42*42,
            35 + 42 + 42*42 + 42*42*42,
            35 + 42 + 42*42 + 42*42*42 + 42*42*42*42,
            35 + 42 + 42*42 + 42*42*42 + 42*42*42*42 + 42*42*42*42*42
    };
    // Ak to vieme priamo z direct sector pointera
    if(n < prefix[0]) {
        uint32_t sector_num = read_number_from_buffer(inode, 3, 6 + 3 * n);
        hdd_read(sector_num, resulting_sector);
        return OK;
    }
    // Musime ist cez indirect bloky
    for(uint32_t indirectness = 1; indirectness <= 5; indirectness++) {
        if(n < prefix[indirectness]) {
            uint32_t indirect_block_num = read_number_from_buffer(inode, 3, 6 + 3*35 + 3*(indirectness - 1));
            return read_tree(indirect_block_num, indirectness, n - prefix[indirectness - 1], resulting_sector);
        }
    }
    // Ak to nespadlo ani do jedneho indirect bloku ani direct bloku, tak n je moc velke (nepodporujeme take velke)
    return FAIL;
}

void inode_set_type(uint8_t* inode, uint8_t type) {
    assert(type == STAT_TYPE_FILE || type == STAT_TYPE_DIR || type == STAT_TYPE_SYMLINK);
    inode[0] &= ~(3 << 6); // vynulujeme predchadzajuci nastaveny typ
    inode[0] |= (type << 6); // nastavime novy
}

void inode_set_num_hard_links(uint8_t* inode, uint16_t num_hard_links) {
    assert(num_hard_links < (1 << 15));
    inode[0] &= (3 << 6); // Nechame typ, ale znulujeme informacie o num_hard_links z nulteho bytu
    inode[0] |= (uint8_t)(num_hard_links >> 8);
    inode[1] = (uint8_t)(num_hard_links % (1 << 8)); // Prvy byte uplne prepiseme
}

void inode_set_size(uint8_t* inode, uint32_t size) {
    uint32_t mask = 255;
    for(uint32_t i = 0, shift_bits = 24; i < 4; i++, shift_bits -= 8) {
        inode[2 + i] = (uint8_t)((size >> shift_bits) & mask);
    }
}

/**
 * Zapise sektor sector ako dest_idx-ty direct sector inodu cislo inode_num
 * Ak dext-idx-ty sektor este nema inode priradeny, tak mu ho priradime
 */
int write_direct(uint32_t inode_num, uint8_t* sector, uint32_t dest_idx) {
    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);

    // Zistime, ci tento subor uz ma dost priradenych direct blokov, alebo musime priradit novy
    uint32_t file_size = inode_get_size(inode);
    uint32_t file_used_blocks = (file_size / SECTOR_SIZE) + (file_size % SECTOR_SIZE ? 1 : 0);
    if(file_used_blocks > dest_idx) {
        // Uz mame dost priradenych blokov, mozeme to rovno zapisat
        uint32_t target_block_num = read_number_from_buffer(inode, 3, 6 + 3 * dest_idx);
        hdd_write(target_block_num, sector);
        return OK;
    }
    // Este nemame dost direct blokov, musime priradit novy
    int free_block_num = get_free_sector_num();
    if(free_block_num == NO_FREE_SECTOR)
        return FAIL; // Uz nie je ziaden volny sektor
    mark_sector((uint32_t)free_block_num, USED); // Oznacime novy sektor za obsadeny
    write_number_to_buffer(inode, 3, 6 + 3 * dest_idx, (uint32_t)free_block_num); // V inode si zaznamename cislo tohto direct bloku
    hdd_write((size_t)free_block_num, sector); // Zapiseme tam nas sektor ktory chceme zapisat
    write_inode(inode_num, inode); // Zaznacime zmeny (informacie o novom priradenom bloku) do inodu
    return OK;
}

/**
 * Povie, ci v inode existuje indirect sector levelu indirectness
 * Ocakava indirectness z rozsahu 1 az 5 vratane
 */
uint8_t exists_indirect(uint32_t inode_num, uint32_t indirectness) {
    assert(1 <= indirectness && indirectness <= 5);

    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);

    uint32_t indirect_num = read_number_from_buffer(inode, 3, 6 + 3 * 35 + 3 * (indirectness - 1));
    return (uint8_t)(indirect_num ? 1 : 0);
}

/**
 * inodu cislo inode_num priradi indirect sector levelu indirectness
 * Predpoklada, ze este tento inode takyto indirect sector nema priradeny a ze 1 <= indirectness <= 5
 */
int assign_indirect(uint32_t inode_num, uint32_t indirectness) {
    assert(1 <= indirectness && indirectness <= 5);
    int free_block_num = get_free_sector_num();
    if(free_block_num == NO_FREE_SECTOR) {
        return FAIL; // Uz nie je ziaden volny sektor na priradenie
    }
    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);
    write_number_to_buffer(inode, 3, 6 + 3 * 35 + 3 * (indirectness - 1), (uint32_t)free_block_num); // Poznacime si to do inodu
    write_inode(inode_num, inode);
    mark_sector((uint32_t)free_block_num, USED);
    return OK;
}

/**
 * Vrati cislo indirect sektora levelu indirectness z inodu cislo inode_num
 * Predpoklada 1 <= indirectness <= 5 a tiez ze taky indirect sector uz je v tom inode priradeny
 */
uint32_t get_indirect_num(uint32_t inode_num, uint32_t indirectness) {
    assert(1 <= indirectness && indirectness <= 5);

    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);

    uint32_t indirect_num = read_number_from_buffer(inode, 3, 6 + 3 * 35 + 3 * (indirectness - 1));
    return indirect_num;
}

/**
 * Bude preliezat rekurzivne strom s korenom sektor cislo indirect_num tak, aby sa dostal do listu,
 * ktory reprezentuje dest-idx-ty sektor v tomto strome (cislovane od 0)
 */
int write_tree(uint32_t indirect_num, uint32_t level, uint32_t dest_idx, uint8_t* sector) {
    uint32_t level_sizes[6] = {1, 42, 42*42, 42*42*42, 42*42*42*42, 42*42*42*42*42};

    // Ak uz sme v liste, tak don zapiseme sector a vratime sa
    if(level == 0) {
        hdd_write(indirect_num, sector);
        return OK;
    }

    uint32_t to_skip = dest_idx / level_sizes[level - 1];
    int child_block_num = get_child_num(indirect_num, to_skip); // Ak neexistuje taky child sector, tak ho vytvori a zapise to na disk
    if(child_block_num == FAIL)
        return FAIL; // Nejake problemy pri child blocku, pravdepodobne neexistoval a uz nebolo miesto na vytvorenie noveho
    int status = write_tree((uint32_t)child_block_num, level - 1, dest_idx - to_skip * level_sizes[level - 1], sector);
    return status;
}

/**
 * Zapise sector ako dest-idx-ty blox v subore reprezentovanom inodom cislo inode_num
 * Ak potrebuje este neexistujuce bloky (take, co treba, ale suboru este nepatria), cestou si ich vytvori
 */
int write(uint32_t inode_num, uint8_t* sector, uint32_t dest_idx) {
    uint32_t prefix[6] = {
            35,
            35 + 42,
            35 + 42 + 42*42,
            35 + 42 + 42*42 + 42*42*42,
            35 + 42 + 42*42 + 42*42*42 + 42*42*42*42,
            35 + 42 + 42*42 + 42*42*42 + 42*42*42*42 + 42*42*42*42*42
    };
    // Ak to vieme vybavit direct sector pointerom, tak to spravime
    if(dest_idx < 35) {
        return write_direct(inode_num, sector, dest_idx);
    }
    // Ak potrebujeme ist cez indirect pointery, tak budeme rekurzivne zliezat po spravnom indirect strome
    // a cestou vytvarat potrebne indirect bloky a finalny sektor, ak este neexistuju
    // Musime ale zistit, ktorym indirect sector pointerom sa z inodu mame najskor vydat
    for(uint32_t i = 1; i <= 5; i++) {
        // Uz chceme ist tymto indirect-stromom, lebo nas sektor ma patrit do neho
        if(dest_idx < prefix[i]) {
            // Ak inode este nema priradeny tento indirect-sector, tak mu nejaky priradime
            if(!exists_indirect(inode_num, i)) {
                if(assign_indirect(inode_num, i) == FAIL) {
                    return FAIL;
                }
            }
            uint32_t indirect_num = get_indirect_num(inode_num, i);
            return write_tree(indirect_num, i, dest_idx - prefix[i - 1], sector); // Zacneme rekurzivne prehladavat spravny indirect strom
        }
    }
    return FAIL; // Ak sa sektor nezmestil ani do jedneho stromu, tak je to prilis neskory sektor (nepodporujeme take velke subory)
}

/**
 * Nastavi polozku first_item_sector v inode typu directory, co je "zaciatocny sektor" linked-listu poloziek v directory
 */
void inode_dir_set_first_item_sector(uint8_t* inode, uint32_t first_sector_num) {
    for(uint32_t i = 0, shift_bits = 16; i < 3; i++, shift_bits -= 8) {
        inode[6 + i] = (uint8_t)(first_sector_num >> shift_bits);
    }
}

/**
 * Nastavi polozku first_item_slot v inode typu directory, co je "zaciatok" linked-listu poloziek v directory
 * first_item_slot hovori, ze linked-list zacina v na slote cislo first_item_slot v bloku first_item_sector, ktory je
 * tiez ulozeny v inode cislo inode_num
 */
void inode_dir_set_first_item_slot(uint8_t* inode, uint8_t first_item_slot) {
    inode[9] = first_item_slot;
}

/**
 * Pozrie sa na direct sector pointery v inode a odznaci vsetky (to, kolko ich je vyuzitych hovori size, takze staci odznacit prvych size)
 */
void unmark_direct(uint8_t* inode, uint32_t size) {
    uint32_t starting_byte = 6;
    for(uint32_t i = 0; i < size; i++) {
        uint32_t sector_num = read_number_from_buffer(inode, 3, starting_byte + 3 * i);
        mark_sector(sector_num, UNUSED);
    }
}

/**
 * Dostane cislo indirect bloku urovne level (teda 2 = double indirect, ...) a vsetky jeho bloky oznaci za
 * nepouzivane, teda rekurzivne prelezie strom dodola a bude odznacovat
 */
void unmark_tree(uint32_t indirect_num, uint32_t level) {
    if(indirect_num == 0) {
        return; // Takyto indirect sektor neexistuje (0 tak interpretujeme)
    }
    if(level == 0) {
        // Uz sme v datovom bloku, tak ho iba oznacime za volny
        mark_sector(indirect_num, UNUSED);
        return;
    }

    uint8_t indirect_block[SECTOR_SIZE];
    hdd_read(indirect_num, indirect_block);

    // Rekurzivne odznacime vsetky deti
    for(uint32_t i = 0; i < 42; i++) {
        uint32_t child_num = read_number_from_buffer(indirect_block, 3, 3 * i);
        unmark_tree(child_num, level - 1);
    }

    // A na koniec odznacime seba
    mark_sector(indirect_num, UNUSED);
}

/**
 * Pozrie sa, ktore bloky vyuziva na data subor reprezentovany inodom inode a vsetky tieto bloky oznaci za nepouzivane
 * Pouziva sa pri skracovani suboru na nulovu velkost a pri mazani
 */
void inode_mark_file_blocks_as_unused(uint32_t inode_num) {
    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);
    int32_t size = inode_get_size(inode);

    unmark_direct(inode, (uint32_t)min(size, 35));
    write_inode(inode_num, inode);

    read_inode(inode_num, inode);

    for(uint32_t indirectness = 1; indirectness <= 5; indirectness++) {
        uint32_t indirect_num = read_number_from_buffer(inode, 3, 6 + 3 * 35 + 3 * (indirectness - 1));
        if(indirect_num == 0) {
            break; // Uz nie je takyto indirect sektor, tak nebudu ani vacsie
        }
        unmark_tree(indirect_num, indirectness);
    }
}

/**
 * Vyplni metadata a podla typu mozno aj nejake dalsie veci
 */
void setup_new_inode(uint32_t inode_num, uint8_t type) {
    assert(type == STAT_TYPE_FILE || type == STAT_TYPE_DIR || type == STAT_TYPE_SYMLINK);
    uint8_t inode[SECTOR_SIZE] = {0};
    // nastavime metadata
    inode_set_type(inode, type);
    inode_set_num_hard_links(inode, 1);
    inode_set_size(inode, 0);

    if(type == STAT_TYPE_FILE) {
        // Suboru vyplnime pointery na dalsie bloky na 00, cim hovorime, ze su este unused
        for(uint32_t i = 6; i < SECTOR_SIZE; i++) {
            write_number_to_buffer(inode, 1, i, 0);
        }
    }
    else if(type == STAT_TYPE_DIR) {
        // Nastavime first_item_sector na NOTHING, lebo este v tomto priecinku nic nie je
        inode_dir_set_first_item_sector(inode, NOTHING);
        inode_dir_set_first_item_slot(inode, 0); // Aby to bolo pekne, tak slot nastavime na 0, aj ked to netreba
    }

    write_inode(inode_num, inode);
    mark_inode(inode_num, USED);
}

/**
 * Vrati cislo bloku, v ktorom je prva polozka v directory reprezentovanom inodom inode
 */
uint32_t inode_dir_get_first_item_sector(uint8_t* inode) {
    return read_number_from_buffer(inode, 3, 6);
}

/**
 * Vrati informaciu, kolkaty slot bloku obsadzuje prva polozka v adresari reprezentovanom inodom inode
 * Slotov v adresari je 6, teda su to 0, 1, 2, 3, 4, 5
 */
uint8_t inode_dir_get_first_item_slot(uint8_t* inode) {
    uint32_t ans = read_number_from_buffer(inode, 1, 9);
    assert(ans == (ans % 256));
    return ((uint8_t)(ans));
}

/**
 * Precita name z polozky v adresari a ulozi ho do name
 */
void dir_entry_get_name(uint32_t sector_num, uint8_t slot_num, char* name) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    for(int i = 0; i < MAX_FILENAME; i++) {
        name[i] = sector[slot_num * SLOT_SIZE + 1 + i];
    }
}

/**
 * Precita inode_num z polozky v adresari (ta polozka je urcena pomocou sector_num a slot_num) a vrati ho
 */
uint32_t dir_entry_get_inode_num(uint32_t sector_num, uint8_t slot_num) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    return read_number_from_buffer(sector, 3, slot_num * SLOT_SIZE + 1 + MAX_FILENAME);
}

void fill_sector_slot(uint32_t sector_num, uint8_t slot, const char* filename, uint32_t inode_num, uint32_t nxt_item_block, uint8_t nxt_item_slot) {
    uint8_t buffer[SECTOR_SIZE];
    hdd_read(sector_num, buffer);
    uint32_t starting_byte = (uint32_t)slot * SLOT_SIZE;

    // Nastavime is_occupied
    buffer[starting_byte] = 1;

    // Nastavime name
    uint32_t name_len = (uint32_t)strlen(filename);
    assert(name_len <= MAX_FILENAME);
    // najskor znulujeme vsetky bajty pre name
    for(uint32_t i = 0; i < MAX_FILENAME; i++) {
        buffer[starting_byte + 1 + i] = 0;
    }
    // Teraz zapiseme samotny filename. Vo vyhradenych MAX_FILENAME bajtoch ho zarovname doprava, takze prvych niekolko bajtov mozno bude nulovych
    uint32_t diff = MAX_FILENAME - name_len;
    for(uint32_t i = 0; i < name_len; i++) {
        buffer[starting_byte + 1 + diff + i] = (uint8_t)filename[i];
    }

    // nastavime inode_num
    write_number_to_buffer(buffer, 3, starting_byte + 1 + MAX_FILENAME, inode_num);

    // nastavime nxt_item_block
    write_number_to_buffer(buffer, 3, starting_byte + 1 + MAX_FILENAME + 3, nxt_item_block);

    // nastavime nxt_item_slot
    write_number_to_buffer(buffer, 1, starting_byte + 1 + MAX_FILENAME + 3 + 3, nxt_item_slot);

    // Upravime num_occupied_slots pre tento sektor
    uint8_t num_occupied_slots = buffer[120]; // Tato informacia je skoro na konci bloku
    num_occupied_slots += 1;
    assert(num_occupied_slots <= 6);
    buffer[120] = num_occupied_slots;

    // Zapiseme zmeny na disk
    hdd_write(sector_num, buffer);
}

/**
 * Vrati cislo sektora nasledujucej polozky v adresari na zaklade sektora a slotu aktualnej polozky
 */
uint32_t dir_get_nxt_item_sector(uint8_t* sector, uint8_t slot) {
    assert(slot <= 5);

    uint32_t slot_start_byte = slot * SLOT_SIZE;

    // Iba si overme, ci je slot naozaj vyuzivany, lebo ak nie, nemali by sme volat tuto funkciu
    // Pozrieme sa v nom na is_occupied
    assert(sector[slot_start_byte] == 1);

    return read_number_from_buffer(sector, 3, slot_start_byte + 1 + MAX_FILENAME + 3);
}

/**
 * Vrati cislo slotu nasledujucej polozky v adresari na zaklade sektora a slotu aktualnej polozky
 */
uint8_t dir_get_nxt_item_slot(uint8_t* sector, uint8_t slot) {
    assert(slot <= 5);

    uint32_t slot_start_byte = slot * SLOT_SIZE;

    // Iba si overme, ci je slot naozaj vyuzivany, lebo ak nie, nemali by sme volat tuto funkciu
    // Pozrieme sa v nom na is_occupied
    assert(sector[slot_start_byte] == 1);

    return (uint8_t)read_number_from_buffer(sector, 1, slot_start_byte + 1 + MAX_FILENAME + 3 + 3);
}

/**
 * Zacne vo first_item_sector na slote first_item_slot a pojde linked-listom suborov v directory az kym nedojde
 * na koniec. Posledny subor ulozi do last_item_sector a last_item_slot
 */
void find_last_item_in_dir(uint32_t first_item_sector, uint8_t first_item_slot, uint32_t* last_item_sector, uint8_t* last_item_slot) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(first_item_sector, sector);
    uint32_t nxt_item_sector = dir_get_nxt_item_sector(sector, first_item_slot);
    if(nxt_item_sector == NOTHING) {
        // Dosli sme na koniec, sme uz v poslednom prvku linked-listu, tak ho ulozime a vratime sa
        *last_item_sector = first_item_sector;
        *last_item_slot = first_item_slot;
    }
    else {
        // Este nie sme na konci, tak ideme rekurzivne do dalsieho prvku linked-listu
        uint8_t nxt_item_slot = dir_get_nxt_item_slot(sector, first_item_slot);
        find_last_item_in_dir(nxt_item_sector, nxt_item_slot, last_item_sector, last_item_slot);
    }
}

/**
 * Pozrie sa v directory na entry urceny (sector_num, slot_num) a nacita name tohto entry do filename
 * Ocakava ze vo filename je prave 12B miesta
 */
void get_entry_filename(uint32_t sector_num, uint8_t slot_num, char* filename) {
    uint8_t buffer[SECTOR_SIZE];
    hdd_read(sector_num, buffer);
    uint32_t starting_byte = slot_num * SLOT_SIZE;
    // Filename je na slote na bajtoch 1 az 13
    for(uint32_t i = 0; i < MAX_FILENAME; i++)
        filename[i] = buffer[starting_byte + 1 + i];
}

/**
 * Dostane filename a prvu polozku v linked-liste entries nejakeho directory.
 * V linked-liste entries v tom directory najde entry ktory je o 1 pred
 * filom filename a tiez entry ktory je o 1 za filom filename.
 * Toto sa da vyuzit pri mazani filename z toho directory, aby sme mohli predchadzajucemu entry
 * nastavit ako nasledujuci prvok v linked-liste ten entry, ktory je o 1 za filom filename
 * Vzdy sme aktualne v right_item_sector v slote right_item_slot
 * Ak na (mid_item_sector, mid_item_slot) je nas filename, tak end=1 a nalavo, resp. napravo
 * su (left_item_sector, left_item_slot), resp. (right_item_sector, right_item_slot)
 * Predpoklada, ze filename existuje v tomto directory
 * V pripade, ze filename je hned prvy, dostaneme *left_item_sector=0
 * V pripade, ze filename je posledny v directory, dostaneme right_item_sector=0
 * Predpokladame, ze filename ma prave 12B (doplnene zlava nulovymi bajtami)
 */
void get_prev_and_next_dir_entries_from_file(char* filename, uint32_t* left_item_sector, uint8_t* left_item_slot,
        uint32_t* mid_item_sector, uint8_t* mid_item_slot, uint32_t* right_item_sector, uint8_t* right_item_slot, uint32_t end) {

    // Nasli sme ich a uz ich mame ulozene v left a right, takze iba sa vratime
    if(end == 1) {
        return;
    }

    // Budeme neskor kontrolovat, ci aktualny filename je ten, co hladame
    char curr_filename[MAX_FILENAME];
    get_entry_filename(*right_item_sector, *right_item_slot, curr_filename);
    uint8_t curr_sector[SECTOR_SIZE];
    hdd_read(*right_item_sector, curr_sector);
    uint32_t nxt_item_sector = dir_get_nxt_item_sector(curr_sector, *right_item_slot);
    // Ak uz nie je dalsi prvok v linked liste, tak urcite sme na nasom subore, lebo predpokladame ze v tomto directory existuje
    // preto to nemusime nejak riesit, lebo vieme, ze nastavime end=1 a v rekurzivnom volani sa hned returneme bez pokusu ist dalej
    uint8_t nxt_item_slot = dir_get_nxt_item_slot(curr_sector, *right_item_slot);
    // Shiftneme left, mid, right o 1 doprava na mid, right, nxt
    *left_item_sector = *mid_item_sector;
    *left_item_slot = *mid_item_slot;
    *mid_item_sector = *right_item_sector;
    *mid_item_slot = *right_item_slot;
    *right_item_sector = nxt_item_sector;
    *right_item_slot = nxt_item_slot;
    get_prev_and_next_dir_entries_from_file(
            filename,
            left_item_sector, left_item_slot,
            mid_item_sector, mid_item_slot,
            right_item_sector, right_item_slot,
            filenames_equal(curr_filename, filename) // Ak sa rovnaju filenames, tak uz sme nasli nas subor a nechceme ist dalej, len nastavit end=1
    );
}

/**
 * Pozrie sa v sektore na polozku num_occupied_slots. Z nej a znalosti poctu slotov na sektor vie zistit, ci je sektor plny
 */
uint8_t dir_sector_is_full(uint32_t sector_num) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    uint32_t num_occupied = read_number_from_buffer(sector, 1, NUM_SLOTS * SLOT_SIZE);
    return (uint8_t)(num_occupied == NUM_SLOTS);
}

/**
 * Zoberie polozku v linked-liste suborov v adresari urcenu sector_num a slot_num a nastavi tejto polozke ako polohu
 * nasledujucej polozky nxt_sector_num a nxt_slot_num
 */
void dir_set_nxt_item_sector_and_slot(uint32_t sector_num, uint8_t slot_num, uint32_t nxt_sector_num, uint8_t nxt_slot_num) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    write_number_to_buffer(sector, 3, slot_num * SLOT_SIZE + 1 + MAX_FILENAME + 3, nxt_sector_num);
    write_number_to_buffer(sector, 1, slot_num * SLOT_SIZE + 1 + MAX_FILENAME + 3 + 3, nxt_slot_num);
    hdd_write(sector_num, sector);
}

/**
 * Zoberie polozku v linked-liste suborov v adresari urcenu sector_num a slot_num a nastavi tejto
 * polozke is_occupied = value, teda 1 ak je obsadena, 0 ak nie je
 */
void dir_set_occupied(uint32_t sector_num, uint8_t slot_num, uint8_t value) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    write_number_to_buffer(sector, 1, slot_num * SLOT_SIZE, value);
    hdd_write(sector_num, sector);
}

uint8_t dir_slot_is_occupied(const uint8_t* sector, uint8_t slot_num) {
    assert(slot_num < NUM_SLOTS);
    return (uint8_t)(sector[slot_num * SLOT_SIZE] == 1);
}

/**
 * Prezrie vsetky sloty v sektore sector_num a vrati cislo prveho volneho
 */
uint8_t dir_find_free_slot(uint32_t sector_num) {
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    for(uint8_t i = 0; i < NUM_SLOTS; i++) {
        if(!dir_slot_is_occupied(sector, i)) {
            return i;
        }
    }
    return NUM_SLOTS;
}

int8_t add_to_directory(uint32_t inode_num, const char* filename, uint32_t new_inode_num) {
    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);
    uint32_t first_item_sector = inode_dir_get_first_item_sector(inode);
    uint8_t first_item_slot = inode_dir_get_first_item_slot(inode);
    // Ak inode este nema ani zaciatok linked-listu (directory je este prazdny)
    if(first_item_sector == NOTHING) {
        int32_t new_sector_num = get_free_sector_num();
        if(new_sector_num == NO_FREE_SECTOR) {
            return FAIL; // Uz nemame volne bloky
        }
        mark_sector((uint32_t)new_sector_num, USED);
        inode_dir_set_first_item_sector(inode, (uint32_t)new_sector_num);
        inode_dir_set_first_item_slot(inode, 0);
        write_inode(inode_num, inode);
        fill_sector_slot((uint32_t)new_sector_num, 0, filename, new_inode_num, NOTHING, 0);
        inode_set_size(inode, inode_get_size(inode) + 1); // Pridali sme vec do directory
        write_inode(inode_num, inode);
        return SUCCESS;
    }
    else {
        // Uz su nejake subory v directory
        uint32_t last_item_sector;
        uint8_t last_item_slot;
        // Vyplni odpoved do last_item_sector a last_item_slot (bude traverzovat linked-list az po koniec)
        find_last_item_in_dir(first_item_sector, first_item_slot, &last_item_sector, &last_item_slot);
        if(dir_sector_is_full(last_item_sector)) {
            // Posledny sektor pouzivany v linked-liste suborov v adresari je uz plny, musime pridat dalsi sektor
            // a do jeho prveho slotu dat informacie o novom subore
            int32_t new_sector_num = get_free_sector_num();
            if(new_sector_num == NO_FREE_SECTOR) {
                return FAIL; // Uz nemame volne sektory
            }
            // Teraz uz new_sector_num je urcite nezaporny
            mark_sector((uint32_t)new_sector_num, USED);
            dir_set_nxt_item_sector_and_slot(last_item_sector, last_item_slot, (uint32_t)new_sector_num, 0);
            fill_sector_slot((uint32_t)new_sector_num, 0, filename, new_inode_num, NOTHING, 0);
            inode_set_size(inode, inode_get_size(inode) + 1); // Pridali sme polozku do directory
            write_inode(inode_num, inode);
            return SUCCESS;
        } else {
            // V poslednom pouzivanom sektore linked-listu suborov v adresari je este volny slot, tak
            // ho najdime a pouzime ho pre nas novy subor
            uint8_t free_slot = dir_find_free_slot(last_item_sector);
//            dir_set_occupied(last_item_sector, free_slot, 1);
            dir_set_nxt_item_sector_and_slot(last_item_sector, last_item_slot, last_item_sector, free_slot); // Pridame linku v linked-liste ku poslednemu suboru
            fill_sector_slot(last_item_sector, free_slot, filename, new_inode_num, NOTHING, 0); // Poznacime si data o novom subore do jeho slotu
            inode_set_size(inode, inode_get_size(inode) + 1); // Uz je v directory o 1 vec viac
            write_inode(inode_num, inode);
            return SUCCESS;
        }
    }
}

// =====================================

// ---------------------------------------------------------------------

/**
 * Naformatovanie disku.
 *
 * Zavola sa vzdy, ked sa vytvara novy obraz disku. 
 */
void fs_format()
{
    uint8_t zero_buffer[SECTOR_SIZE] = {0};
    uint8_t full_buffer[SECTOR_SIZE];
    for(size_t i = 0; i < SECTOR_SIZE; i++) {
        full_buffer[i] = 255;
    }

    // Vyplnime cely disk nulami
    for(size_t i = 0; i < num_sectors(); i++) {
        hdd_write(i, zero_buffer);
    }

    // Oznacime v bitmape blokov pouzite bloky (tie co sa pouzivaju na bitmapy blokov a inodov + tie co su vyhradene pre inody)
    size_t num_bm_set_bits = sector_bm_len() + inode_bm_len() + num_inodes(); // Kolko prvych bitov bitmapy blokov ma byt 1
    size_t num_used_sectors = num_bm_set_bits / (8 * SECTOR_SIZE); // Kolko prvych sektorov su same 1
    size_t num_rem_bits = num_bm_set_bits % (8 * SECTOR_SIZE); // Kolko prvych bitov je 1 z prveho takeho sektora, co nie su same 1
    size_t num_rem_bytes = num_rem_bits / 8; // Kolko prvych bajtov toho sektora su same 1
    uint8_t last_byte_bits = (uint8_t)(num_rem_bits % 8); // Kolko prvych bitov posledneho nenuloveho bajtu v poslednom nenulovom sektore ma byt 1
    for(size_t i = 0; i < num_used_sectors; i++) {
        hdd_write(i, full_buffer);
    }
    uint8_t last_sector[SECTOR_SIZE] = {0};
    // Vyplnime jednotkami spravny prefix posledneho sektora ktory ma byt nenulovy
    for(size_t i = 0; i < num_rem_bytes; i++) {
        last_sector[i] = 255;
    }
    // Vyplnime jednotkami spravny prefix posledneho bajtu, ktory ma byt nenulovy
    for(uint8_t i = 0, set_bit = 7; i < last_byte_bits; i++, set_bit--) {
        last_sector[num_rem_bytes] |= (1 << set_bit);
    }
    hdd_write(num_used_sectors, last_sector);

    // Nastavime inode 0, pretoze ten bude pre /
    setup_new_inode(0, STAT_TYPE_DIR);
}

/**
 * Predpokladame, ze wanted_name vzdy dostaneme ako pole MAX_FILENAME charov
 */
int32_t get_inode_num_from_dir_recursive(uint32_t sector_num, uint8_t slot_num, const char* wanted_name) {
    char curr_name[MAX_FILENAME];
    dir_entry_get_name(sector_num, slot_num, curr_name); // Do curr_name si nacitame meno aktualnej polozky (aktualneho suboru v linked_liste)
    // Ak sme nasli polozku so spravnym menom, precitame cislo jej inodu a vratime ho
    if(filenames_equal(curr_name, wanted_name)) {
        return (int32_t)dir_entry_get_inode_num(sector_num, slot_num);
    }
    uint8_t sector[SECTOR_SIZE];
    hdd_read(sector_num, sector);
    uint32_t nxt_sector_num = dir_get_nxt_item_sector(sector, slot_num);
    if(nxt_sector_num == NOTHING) {
        return FAIL;
    }
    uint8_t nxt_slot_num = dir_get_nxt_item_slot(sector, slot_num);
    return get_inode_num_from_dir_recursive(nxt_sector_num, nxt_slot_num, wanted_name);
}

/**
 * Prezrie polozky v adresari (bude prechadzat linked-list suborov v adresari) urcenom inodom dir_inode_num
 * Ak najde subor s nazvom name, vrati cislo jeho inodu
 * Ak nenajde, vrati FAIL
 */
int32_t get_inode_num_from_dir(uint32_t dir_inode_num, const char* wanted_name) {
    assert(strlen(wanted_name) <= MAX_FILENAME);

    char aligned_wanted_name[MAX_FILENAME] = {0}; // Zarovname si name na MAX_FILENAME bajtov, aby to bolo rovnako dlhe ako name v directory entry slote
    uint8_t diff = (uint8_t)(MAX_FILENAME - strlen(wanted_name));
    for(int i = diff; i < MAX_FILENAME; i++) {
        aligned_wanted_name[i] = wanted_name[i - diff];
    }

    uint8_t inode[SECTOR_SIZE];
    read_inode(dir_inode_num, inode);
    if(inode_get_type(inode) != STAT_TYPE_DIR) {
        return FAIL;
    }
    uint32_t first_item_sector = inode_dir_get_first_item_sector(inode);
    // Ak nie su ziadne polozky v directory, tak sme nic nenasli
    if(first_item_sector == NOTHING) {
        return FAIL;
    }
    uint8_t first_item_slot = inode_dir_get_first_item_slot(inode);
    int32_t inode_num = get_inode_num_from_dir_recursive(first_item_sector, first_item_slot, aligned_wanted_name); // Ak nenajde, tak toto bude -1
    return inode_num;
}

/**
 * Prechadza rekurzivne cez directories v path a ked najde spravny, vytvori v nom subor
 * Vrati FAIL ak sa nepodari, inode number noveho suboru ak sa podari
 */
int32_t fs_creat_recursive(char *path, uint32_t inode_num) {
    if(strchr(path, '/') == NULL) {
        // Uz mame v path iba filename
        int32_t file_inode_num = get_inode_num_from_dir(inode_num, path);
        // Ak subor uz existuje
        if(file_inode_num != FAIL) {
            // Ideme ho skracovat na prazdny, takze odznacime vsetky jeho bloky a potom nastavime jeho size na 0 (v inode)
            uint8_t inode[SECTOR_SIZE];
            read_inode(inode_num, inode);
            inode_mark_file_blocks_as_unused(inode_num);
            inode_set_size(inode, 0);
            return inode_num;
        }
        else {
            // Este neexistuje, priradime mu inode
            uint32_t new_inode_num = get_free_inode_num();
            if(new_inode_num == NO_FREE_INODE) {
                return FAIL;
            }
            setup_new_inode(new_inode_num, STAT_TYPE_FILE);
            add_to_directory(inode_num, path, new_inode_num);
            return new_inode_num;
        }
    }
    // Este musime prejst cez nejaky directory
    char* path_copy = strdup(path);
    char* path_copy_copy = path_copy;
    char* dir_name = strtok(path_copy, "/");
    int32_t nxt_inode_num = get_inode_num_from_dir(inode_num, dir_name);
    if(nxt_inode_num == -1) {
        return FAIL; // Neexistuje taka polozka v ceste dalej
    }
    uint8_t nxt_inode[SECTOR_SIZE];
    read_inode((uint32_t)nxt_inode_num, nxt_inode);
    if(inode_get_type(nxt_inode) != STAT_TYPE_DIR) {
        return FAIL; // Nasli sme subor, ale nie je to directory
    }
    // Zmazeme dalsi directory v ceste a rekurzivne ideme vyriesit zvysok
    free(path_copy_copy);
    path_copy = strdup(path);
    path_copy_copy = path_copy;
    while(path_copy[0] != '/')
        path_copy++;
    path_copy++; // Este zmazeme /

    int32_t ret = fs_creat_recursive(path_copy, (uint32_t)nxt_inode_num); // Teraz uz bude nxt_inode_num urcite nezaporne

    free(path_copy_copy);

    return ret;
}

/**
 * Vytvorenie suboru.
 *
 * Volanie vytvori v suborovom systeme na zadanej ceste novy subor a vrati
 * handle nan. Ak subor uz existoval, bude skrateny na prazdny. Pozicia v subore bude
 * nastavena na 0ty byte. Ak adresar, v ktorom subor ma byt ulozeny, neexistuje,
 * vrati FAIL (sam nevytvara adresarovu strukturu, moze vytvarat iba subory).
 */
file_t* fs_creat(const char *path)
{
    char* path_copy = strdup(path);
    char* path_copy_copy = path_copy;
    path_copy++; // Zbavime sa / na zaciatku
    int32_t inode_num = fs_creat_recursive(path_copy, 0); // Zacneme z inode 0, co je vzdy inode pre /
    if(inode_num == FAIL)
        return NULL;
    uint8_t inode[SECTOR_SIZE];
    read_inode((uint32_t)inode_num, inode);

    free(path_copy_copy);
    return fs_open(path);
}

file_t* setup_handle(uint32_t inode_num, uint32_t position) {
    file_t* fd = fd_alloc();
    handle_set_inode(fd, inode_num);
    handle_set_position(fd, position);
    return fd;
}

// Z inodu pre symlink sa pozrie na cestu ktoru symlink reprezentuje
void read_path_from_symlink(const uint8_t* inode, char* symlink_path) {
    for(int i = 0; i < MAX_PATH; i++) {
        symlink_path[i] = inode[6 + i];
    }
}

file_t* fs_open_recursive(char* path, uint32_t inode_num) {
    if(strchr(path, '/') == NULL) {
        // Uz mame v path iba filename
        int32_t file_inode_num = get_inode_num_from_dir(inode_num, path);
        // Ak subor existuje
        if(file_inode_num != -1) {
            uint8_t file_inode[SECTOR_SIZE];
            read_inode((uint32_t)file_inode_num, file_inode);
            if(inode_get_type(file_inode) == STAT_TYPE_SYMLINK) {
                char symlink_path[MAX_PATH];
                read_path_from_symlink(file_inode, symlink_path);
                return fs_open(symlink_path);
            }
            if(inode_get_type(file_inode) != STAT_TYPE_FILE) {
                return NULL;
            }
            return setup_handle((uint32_t)file_inode_num, 0); // Nastavime inode number=file_inode_num a position=0
        }
        else {
            // Subor neexistuje
            return NULL;
        }
    }
    // Este musime prejst cez nejaky directory
    char* path_copy = strdup(path);
    char* path_copy_copy = path_copy;
    char* dir_name = strtok(path_copy, "/");
    int32_t nxt_inode_num = get_inode_num_from_dir(inode_num, dir_name);
    if(nxt_inode_num == -1) {
        return NULL;
    }
    uint8_t nxt_inode[SECTOR_SIZE];
    read_inode((uint32_t)nxt_inode_num, nxt_inode);
    if(inode_get_type(nxt_inode) != STAT_TYPE_DIR) {
        return NULL;
    }

    free(path_copy_copy);
    path_copy = strdup(path);
    path_copy_copy = path_copy;
    // Zmazeme dalsi directory v ceste a rekurzivne ideme vyriesit zvysok
    while(path_copy[0] != '/')
        path_copy++;
    path_copy++; // Este zmazeme /
    file_t* ret = fs_open_recursive(path_copy, (uint32_t)nxt_inode_num); // Teraz uz bude nxt_inode_num urcite nezaporne

    free(path_copy_copy);

    return ret;
}

/**
 * Otvorenie existujuceho suboru.
 *
 * Ak zadany subor existuje, funkcia ho otvori a vrati handle nan. Pozicia v
 * subore bude nastavena na 0-ty bajt. Ak subor neexistuje, vrati FAIL.
 */
file_t* fs_open(const char* path)
{
   char* path_copy = strdup(path);
   char* path_copy_copy = path_copy;
   path_copy++; // Zbavime sa / na zaciatku, toho inode uz vieme ze je 0
   file_t* ret = fs_open_recursive(path_copy, 0); // inode_num = 0, lebo zaciname v /

   free(path_copy_copy);

   return ret;
}

/**
 * Zatvori otvoreny file handle.
 *
 * Funkcia zatvori handle, ktory bol vytvoreny pomocou volania 'open' alebo
 * 'creat' a uvolni prostriedky, ktore su s nim spojene. V pripade akehokolvek
 * zlyhania vrati FAIL.
 */
int fs_close(file_t *fd)
{
	/* Uvolnime filedescriptor, aby sme neleakovali pamat */
	fd_free(fd);
	return OK;
}

int32_t get_inode_num(char*); // Aby ju poznal symlink_get_target_inode_num

/**
 * Resolvne symlink a vrati konecne cislo inodu
 * Predpoklada, ze dostane validny inode typu symlink
 */
int32_t symlink_get_target_inode_num(const uint8_t* inode) {
    char symlink_path[MAX_PATH];
    for(int i = 6; i < 6 + MAX_PATH; i++) {
        symlink_path[i - 6] = (char)inode[i];
    }
    int32_t result_inode_num = get_inode_num(symlink_path);
    return result_inode_num; // Toto moze byt aj fail
}

/**
 * Pomocna funkcia pre get_inode_num
 * Ak existuje z inodu curr_inode_num cesta rem_path, tak vrati inode_number posledneho suboru/directory/cohokolvek
 * na tejto ceste. Ak neexistuje, vrati FAIL;
 */
int32_t get_inode_num_recursive(char* rem_path, uint32_t curr_inode_num) {
    // Ak uz sme presli celu path, sme na konci
    if(strcmp(rem_path, "") == 0) {
        return curr_inode_num;
    }
    // FIXME tu treba spravne freenut, ale neviem ako to spravit, nejak mi to nefunguje ked sa snazim robit free(rem_path_copy)
    char* rem_path_copy = strdup(rem_path);
    char* nxt_name = strtok(rem_path_copy, "/");

    rem_path_copy = strdup(rem_path); // Obnovime povodnu hodnotu
    char* rem_path_copy_copy = rem_path_copy;

    int32_t nxt_inode_num = get_inode_num_from_dir(curr_inode_num, nxt_name); // Zistime inode number dalsej veci na ceste
    free(nxt_name);
    if(nxt_inode_num == FAIL) {
        return FAIL; // Neexistuje taka cesta
    }

    uint8_t nxt_inode[SECTOR_SIZE];
    read_inode((uint32_t)nxt_inode_num, nxt_inode);

    uint32_t nxt_type = inode_get_type(nxt_inode);

    if(nxt_type == STAT_TYPE_SYMLINK) {
        nxt_inode_num = symlink_get_target_inode_num(nxt_inode); // Resolvneme symlink na ceste
    }

    // Zmazeme dalsi directory v ceste a rekurzivne ideme vyriesit zvysok
    while (strlen(rem_path_copy) > 0 && rem_path_copy[0] != '/')
        rem_path_copy++;
    if(strlen(rem_path_copy) > 0)
        rem_path_copy++; // Zmazeme zaciatocne /

    int32_t ret = get_inode_num_recursive(rem_path_copy, (uint32_t)nxt_inode_num);  // Rekurzivne vyriesime zvysok cesty z dalsieho inodu v ceste

    free(rem_path_copy_copy);

    return ret;
}

/**
 * podla path najde cislo inodu suboru/directory/cohokolvek na danej path
 * Vracia FAIL ak sa nepodari
 */
int32_t get_inode_num(char* path) {
    if(strcmp(path, "/") == 0)
        return 0;
    uint32_t root_inode_num = 0;  // Toto plati vzdy, lebo / ma inode 0
    char* path_copy = strdup(path);
    char* path_copy_copy = path_copy;
    path_copy++; // odstranime / na zaciatku
    int32_t ret = get_inode_num_recursive(path_copy, root_inode_num);

    free(path_copy_copy);

    return ret;
}

/**
 * Odstrani subor s nazvom filename z  priecinka reprezentovaneho inodom cislo dir_inode_num
 * Predpoklada, ze taky file existuje (treba to overit predtym, ako sa pouzije tato funkcia)
 * Ak clear_space, tak cielom je odstranit entry pre filename z tohto priecinka, oznacit vsetky pouzivane bloky
 * toho filu za volne a tiez oznacit inode toho filu za volny
 * Ak clear_space = 0, tak neuvolnime inode ani bloky (subor teda ostane na disku a iba sa zmaze entry z directory)
 * Ak clear_space == 1, tak uvolnime inode aj bloky
 * Ak clear_space == 2, tak uvolnime iba inode (pouziva sa pri odstranovani empty directory)
 */
void remove_from_dir(uint32_t dir_inode_num, char* filename, uint32_t file_inode_num, uint8_t clear_space) {
    // Treba zmazat entry pre file z directory
    uint8_t dir_inode[SECTOR_SIZE];
    read_inode(dir_inode_num, dir_inode);
    // Zacneme v prvom
    uint32_t nxt_item_sector = inode_dir_get_first_item_sector(dir_inode);
    uint8_t nxt_item_slot = inode_dir_get_first_item_slot(dir_inode);
    uint32_t prev_item_sector = 0, mid_item_sector = 0;
    uint8_t prev_item_slot = 0, mid_item_slot = 0;

    // Doplnime filename nulovymi bajtami zlava, aby mal 12B (tak je ulozeny v directory entry)
    char aligned_filename[MAX_FILENAME] = {0};
    uint32_t small_len = (uint32_t)strlen(filename);
    uint32_t diff = MAX_FILENAME - small_len;
    for(uint32_t i = 0; i < small_len; i++) {
        aligned_filename[diff + i] = filename[i];
    }

    get_prev_and_next_dir_entries_from_file(
            aligned_filename,
            &prev_item_sector, &prev_item_slot,
            &mid_item_sector, &mid_item_slot,
            &nxt_item_sector, &nxt_item_slot,
            0
    );

    // Nastavime ze miesto v directory uz nie je occupied
    dir_set_occupied(mid_item_sector, mid_item_slot, 0);

    if(prev_item_sector == 0 && nxt_item_sector == 0) {
        // filename bol jediny subor v directory, takze iba nastavime ze tam nie su subory
        inode_dir_set_first_item_sector(dir_inode, 0);
        inode_dir_set_first_item_slot(dir_inode, 0);
    }
    else if(prev_item_sector == 0) {
        // filename bol hned prvy, takze chceme nastavit first_item na nxt
        inode_dir_set_first_item_sector(dir_inode, nxt_item_sector);
        inode_dir_set_first_item_slot(dir_inode, nxt_item_slot);
    }
    else if(nxt_item_sector == 0) {
        // filename bol posledny v directory, chceme nastavit prev -> 0
        uint8_t buf[SECTOR_SIZE];
        hdd_read(prev_item_sector, buf);
        dir_set_nxt_item_sector_and_slot(prev_item_sector, prev_item_slot, 0, 0);
        hdd_write(prev_item_sector, buf);
    }
    else {
        // filename ma pred sebou aj za sebou entry, spravime z prev -> mid -> nxt uz iba prev -> nxt
        dir_set_nxt_item_sector_and_slot(prev_item_sector, prev_item_slot, nxt_item_sector, nxt_item_slot);
    }


    // Pre regular files
    if(clear_space == 1) {
        uint8_t file_inode[SECTOR_SIZE];
        read_inode(file_inode_num, file_inode);

        uint32_t num_hard_links = inode_get_num_hardlinks(file_inode);

        // Ak uz to bol posledny hardlink, tak mozeme uvolnit bloky a inode
        if(num_hard_links == 1) {
            // Teraz este odznacit bloky ktore file vyuzival a inode ktory patril filu
            inode_mark_file_blocks_as_unused(file_inode_num);
            mark_inode(file_inode_num, UNUSED);
        }
        else {
            assert(num_hard_links > 1);
            assert(num_hard_links < (1 << 16));
            inode_set_num_hard_links(file_inode, (uint16_t)(num_hard_links - 1));
            write_inode(file_inode_num, file_inode);
        }
    }
    // Pre directory
    else if(clear_space == 2) {
        mark_inode(file_inode_num, UNUSED);
    }

    inode_set_size(dir_inode, inode_get_size(dir_inode) - 1); // Zmazali sme 1 polozku z directory

    // Zapiseme zmeny na disk
    write_inode(dir_inode_num, dir_inode);
}

uint32_t get_filename_len_from_path(const char* path) {
    uint32_t filename_len = 0;
    // Zratame si dlzku filenamu
    for(int32_t i = (int32_t)strlen(path) - 1; i >= 0; i--) {
        if(path[i] == '/')
            break;
        filename_len++;
    }
    return filename_len;
}

/**
 * Odstrani subor na ceste 'path'.
 *
 * Ak zadana cesta existuje a je to subor, odstrani subor z disku; nemeni
 * adresarovu strukturu. V pripade chyby vracia FAIL.
 */
int fs_unlink(const char *path)
{
    char* path_copy = strdup(path);

    uint32_t filename_len = get_filename_len_from_path(path);
    uint32_t parent_path_len = (uint32_t)strlen(path) - filename_len;

    // Ulozime si filename toho suboru ktory chceme zmazat
    char* filename = calloc(filename_len + 1, sizeof(char)); // +1 na \0
    for(uint32_t i = (uint32_t)strlen(path) - 1; i >= parent_path_len; i--) {
        filename[i - parent_path_len] = path[i];
    }

    // Ulozime si cestu k parent directory toho suboru ktory chceme zmazat
    char* parent_path = calloc(parent_path_len + 1, sizeof(char)); // +1 na \0
    for(uint32_t i = 0; i < parent_path_len; i++)
        parent_path[i] = path[i];

    int32_t parent_dir_inode_num = get_inode_num(parent_path);
    if(parent_dir_inode_num == FAIL)
        return FAIL;
    int32_t file_inode_num = get_inode_num(path_copy);
    if(file_inode_num == FAIL)
        return FAIL;
    if(inode_get_type_by_num((uint32_t)file_inode_num) != STAT_TYPE_FILE)
        return FAIL;

    // Zmazeme entry pre nas file z directory v ktorom je a oznacime inode toho suboru za volnya jeho bloky za volne
    remove_from_dir((uint32_t)parent_dir_inode_num, filename, (uint32_t)file_inode_num, 1);

    free(path_copy);
    free(filename);
    free(parent_path);

    return OK;
}

/**
 * Premenuje/presunie polozku v suborovom systeme z 'oldpath' na 'newpath'.
 *
 * Po uspesnom vykonani tejto funkcie bude subor, ktory doteraz existoval na
 * 'oldpath' dostupny cez 'newpath' a 'oldpath' prestane existovat. Opat,
 * funkcia nemanipuluje s adresarovou strukturou (nevytvara nove adresare
 * z cesty newpath, okrem posledneho v pripade premenovania adresara).
 * V pripade zlyhania vracia FAIL.
 */
int fs_rename(const char *oldpath, const char *newpath)
{
    char* old_path_copy = strdup(oldpath);
    char* new_path_copy = strdup(newpath);

    // Pozrieme sa ci existuje subor na oldpath
    int32_t inode_num = get_inode_num(old_path_copy);
    if(inode_num == FAIL)
        return FAIL;

    // Zistime parent directory a filename toho suboru co je na newpath
    uint32_t filename_new_len = get_filename_len_from_path(new_path_copy);
    uint32_t parent_new_path_len = (uint32_t)strlen(new_path_copy) - filename_new_len;
    char parent_new_path[MAX_PATH] = {0};
    for(uint32_t i = 0; i < parent_new_path_len; i++) {
        parent_new_path[i] = new_path_copy[i];
    }
    char filename_new[MAX_FILENAME] = {0};
    for(uint32_t i = 0; i < filename_new_len; i++) {
        filename_new[i] = new_path_copy[parent_new_path_len + i];
    }

    // Pozrieme sa, ci existuje directory struktura pre newpath (teda cele newpath okrem filename)
    int32_t new_dir_inode_num = get_inode_num(parent_new_path);
    if(new_dir_inode_num == FAIL)
        return FAIL;

    // Zistime parent directory a filename toho suboru co je na oldpath
    uint32_t filename_old_len = get_filename_len_from_path(old_path_copy);
    uint32_t parent_old_path_len = (uint32_t)strlen(old_path_copy) - filename_old_len;
    char parent_old_path[MAX_PATH] = {0};
    for(uint32_t i = 0; i < parent_old_path_len; i++) {
        parent_old_path[i] = old_path_copy[i];
    }
    char filename_old[MAX_FILENAME] = {0};
    for(uint32_t i = 0; i < filename_old_len; i++) {
        filename_old[i] = old_path_copy[parent_old_path_len + i];
    }

    // Pozrieme sa, ci existuje directory struktura pre oldpath (teda cele oldpath okrem filename)
    int32_t old_dir_inode_num = get_inode_num(parent_old_path);
    if(old_dir_inode_num == FAIL)
        return FAIL;

    // Teraz zmazeme newpath pre pripad, ze by tam uz nieco existovalo
    fs_unlink(newpath);

    // Subor z oldpath dame do noveho directory
    add_to_directory((uint32_t)new_dir_inode_num, filename_new, (uint32_t)inode_num);

    // Teraz ho odstranime zo stareho
    remove_from_dir((uint32_t)old_dir_inode_num, filename_old, (uint32_t)inode_num, 0);  // 0 lebo nechceme mazat subor

    free(old_path_copy);
    free(new_path_copy);

	return OK;
}

/**
 * Nacita z aktualnej pozicie vo 'fd' do bufferu 'bytes' najviac 'size' bajtov.
 *
 * Z aktualnej pozicie v subore precita funkcia najviac 'size' bajtov; na konci
 * suboru funkcia vracia 0. Po nacitani dat zodpovedajuco upravi poziciu v
 * subore. Vrati pocet precitanych bajtov z 'bytes', alebo FAIL v pripade
 * zlyhania. Existujuci subor prepise.
 */
int fs_read(file_t *fd, uint8_t *bytes, size_t size)
{
    uint32_t inode_num = handle_get_inode_num(fd);
    uint32_t position = handle_get_position(fd); // Kolkaty bajt filu je prvy, co chceme precitat

    // Pozrieme sa, ci inode cislo inode_num naozaj reprezentuje nejaky subor
    if(!inode_used(inode_num))
        return FAIL;

    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);

    // Ak to nie je subor, tak ho nechceme citat
    uint32_t file_type = inode_get_type(inode);
    if(file_type != STAT_TYPE_FILE)
        return FAIL;

    uint32_t file_sz = inode_get_size(inode);
    uint32_t remaining_size = (uint32_t)min(size, file_sz - position);  // Kolko chceme skutocne precitat

    // Zratame si informacie o prvom sektore, z ktoreho ideme citat
    // Chceme asi precitat nejaky jeho suffix
    uint32_t starting_sector_idx = position / SECTOR_SIZE; // Kolkaty sektor v ramci filu je nas zaciatok
    uint32_t first_sector_from = position % SECTOR_SIZE; // Od ktorej pozicie v prvom sektore chceme citat

    // Zratame si informacie o poslednom sektore z ktoreho chceme citat
    // Chceme precitat nejaky jeho prefix
    if(remaining_size == 0)
        return 0; // Chceme citat 0 bajtov, tak ich rovno nacitame a vratime (teda nic nespravime)
    uint32_t last_position = position + remaining_size - 1;
    uint32_t last_sector_idx = last_position / SECTOR_SIZE;
    uint32_t last_sector_to = last_position % SECTOR_SIZE;

    uint8_t curr_sector[SECTOR_SIZE];
    inode_read_nth_sector(inode, starting_sector_idx, curr_sector); // Nacitame prvy sektor co nas zaujima

    // Ak ale chceme dokopy citat iba z jedneho sektora, tak to vyriesime samostatne ako specialny pripad tu
    if(starting_sector_idx == last_sector_idx) {
        assert(first_sector_from <= last_sector_to);
        for(uint32_t i = first_sector_from, bytes_pos = 0; i <= last_sector_to; i++, bytes_pos++) {
            bytes[bytes_pos] = curr_sector[i];
        }
        handle_set_position(fd, (uint32_t)(position + remaining_size));
        return (uint32_t)remaining_size; // Precitali sme vsetko co sme chceli
    }

    // Teraz uz urcite chceme citat z roznych sektorov, teda to bude najskor suffix prveho, potom niekolko
    // celych a potom prefix posledneho

    uint32_t bytes_pos = 0; // Index do bytes

    // Precitame nejaku cast z prveho sektora ktory nas zaujima
    // To bude nejaky jeho suffix
    for(uint32_t i = first_sector_from; i < SECTOR_SIZE; i++) {
        bytes[bytes_pos] = curr_sector[i];
        bytes_pos++;
    }

    // Teraz nacitame vsetky tie cele stredne sektory
    for(uint32_t curr_sector_idx = starting_sector_idx + 1; curr_sector_idx < last_sector_idx; curr_sector_idx++) {
        inode_read_nth_sector(inode, curr_sector_idx, curr_sector);
        for(uint32_t i = 0; i < SECTOR_SIZE; i++) {
            bytes[bytes_pos] = curr_sector[i];
            bytes_pos++;
        }
    }

    // Teraz nacitame nejaky prefix posledneho sektora
    inode_read_nth_sector(inode, last_sector_idx, curr_sector);
    for(uint32_t i = 0; i <= last_sector_to; i++) {
        bytes[bytes_pos] = curr_sector[i];
        bytes_pos++;
    }

    // Posunieme poziciu vo fd
    handle_set_position(fd, position + remaining_size);

    return remaining_size;
}

/**
 * Zapise do 'fd' na aktualnu poziciu 'size' bajtov z 'bytes'.
 *
 * Na aktualnu poziciu v subore zapise 'size' bajtov z 'bytes'. Ak zapis
 * presahuje hranice suboru, subor sa zvacsi; ak to nie je mozne, zapise sa
 * maximalny mozny pocet bajtov. Po zapise korektne upravi aktualnu poziciu v
 * subore a vracia pocet zapisanych bajtov z 'bytes'.
 * 
 * Write existujuci obsah suboru prepisuje, nevklada dovnutra nove data.
 * Write pre poziciu tesne za koncom existujucich dat zvacsi velkost suboru.
 */

int fs_write(file_t *fd, const uint8_t *bytes, size_t size)
{
    uint32_t position = handle_get_position(fd);
    uint32_t inode_num = handle_get_inode_num(fd);

    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);

    uint32_t file_size = inode_get_size(inode);
    uint32_t num_file_sectors = (file_size / SECTOR_SIZE) + (file_size % SECTOR_SIZE > 0 ? 1 : 0);

    // Najdeme index prveho sektora, do ktoreho ideme zapisovat (index v ramci suboru, teda nie cislo sektora na disku)
    uint32_t first_sector_idx = (position / SECTOR_SIZE); // Z tohto sektora chceme asi precitat nejaky suffix (ak nie je v rovnakom sektore aj koniec)
    uint32_t first_from = position % SECTOR_SIZE; // Od ktoreho bajtu budeme citat v prvom sektore ktory nas zaujima

    // Najdeme index posledneho sektora, do ktoreho ideme zapisovat
    uint32_t last_sector_idx = ((position + (uint32_t)size) / SECTOR_SIZE);
    uint32_t last_to = (position + (uint32_t)size) % SECTOR_SIZE; // last_to uz do intervalu nepatri

    // Ak subor este nema ten prvy sektor, tak pouzijeme nulovy, inac nacitame prvy sektor
    uint8_t curr_sector[SECTOR_SIZE] = {0};
    if(first_sector_idx < num_file_sectors) {
        inode_read_nth_sector(inode, first_sector_idx, curr_sector); // Nacitame prvy sektor ktory nas zaujima
    }

    // Ak by sme chceli zapisovat od zaciatku dalsieho bloku (o 1 za koncom suboru), tak este nemusi byt alokovany
    if(first_from == 0) {
        int free_block_num = get_free_sector_num();
        if(free_block_num == NO_FREE_SECTOR) {
            return 0; // Ak sa neda alokovat, nevieme tu nic zapisat
        }
    }
    // Ked vsetko zapisujeme v ramci jedneho sektora, osetrime to ako specialny pripad
    // Kedze zapisujeme v jednom sektore, tak uz ho ten subor musel mat (alebo sme overili, ze sa este da alokovat
    // novy sektor v ife hned nad tymto textom), teda urcite vieme zapisat vsetkych size bajtov
    if(first_sector_idx == last_sector_idx) {
        for(uint32_t i = first_from, j = 0; i < last_to; i++, j++) {
            curr_sector[i] = bytes[j];
        }
        int ret = write(inode_num, curr_sector, first_sector_idx); // Zapiseme zmeny na disk
        read_inode(inode_num, inode); // Updatneme si inode, lebo write ho asi nejak zmenil
        assert(ret != FAIL);
        // Este updatneme size suboru
        inode_set_size(inode, (uint32_t)max(file_size, position + size));
        write_inode(inode_num, inode);
        handle_set_position(fd, position + last_to - first_from);
        return (uint32_t)size;
    }

    uint32_t num_bytes_written = 0;

    // Teraz vseobecny pripad, najskor zapiseme suffix nejakeho sektora, potom niekolko celych sektorov a
    // potom prefix posledneho sektora

    // Najskor zapiseme suffix prveho zaujimaveho sektora
    uint32_t bytes_idx = 0;
    for(int i = first_from; i < SECTOR_SIZE; i++) {
        curr_sector[i] = bytes[bytes_idx];
        bytes_idx++;
        num_bytes_written++;
    }
    write(inode_num, curr_sector, first_sector_idx); // Zapiseme prvy zmeneny sektor na disk

    // Teraz zapiseme niekolko celych sektorov
    for(uint32_t sector_idx = first_sector_idx + 1; sector_idx < last_sector_idx; sector_idx++) {
        for(int i = 0; i < SECTOR_SIZE; i++) {
            curr_sector[i] = bytes[bytes_idx];
            bytes_idx++;
        }
        int ret = write(inode_num, curr_sector, sector_idx); // Zapiseme cely tento sektor na disk
        if(ret == FAIL) {
            // Uz nemame miesto, musime skoncit
            read_inode(inode_num, inode); // Updatneme si inode. Nie som si isty, ci to tu treba, asi nie, ale radsej to tu zatial necham
            inode_set_size(inode, (uint32_t)max(file_size, position + num_bytes_written)); // Updatneme size suboru
            write_inode(inode_num, inode);
            handle_set_position(fd, position + num_bytes_written);
            return num_bytes_written;
        }
        num_bytes_written += SECTOR_SIZE;
    }

    // Teraz zapiseme prefix posledneho zaujimaveho sektora

    // Ak posledny sektor este suboru nepatri, tak pouzijeme nulovy, inac ideme menit ten co tam uz je
    if(last_sector_idx < num_file_sectors) {
        inode_read_nth_sector(inode, last_sector_idx, curr_sector); // Nacitame posledny zaujimavy sektor do curr_sector
    }
    else {
        for(uint32_t i = 0; i < SECTOR_SIZE; i++)
            curr_sector[i] = 0;
    }

    for(uint32_t i = 0; i < last_to; i++) { // last_to uz do toho intervalu nepatri
        curr_sector[i] = bytes[bytes_idx];
        bytes_idx++;
    }
    int ret = write(inode_num, curr_sector, last_sector_idx); // Zapiseme posledny zmeneny sektor na disk
    if(ret != FAIL) {
        // Este sa podarilo zapisat posledny sektor, lebo asi bolo miesto
        num_bytes_written += last_to;
    }

    read_inode(inode_num, inode); // Updatneme si inode
    inode_set_size(inode, (uint32_t)max(file_size, position + num_bytes_written)); // Este updatneme size suboru
    write_inode(inode_num, inode);

    handle_set_position(fd, position + num_bytes_written);

    return num_bytes_written;
}

/**
 * Zmeni aktualnu poziciu v subore na 'pos'-ty byte.
 *
 * Upravi aktualnu poziciu; ak je 'pos' mimo hranic suboru, vrati FAIL a pozicia
 * sa nezmeni, inac vracia OK.
 */
int fs_seek(file_t *fd, size_t pos)
{
    uint32_t inode_num = handle_get_inode_num(fd);

    uint8_t inode[SECTOR_SIZE];
    read_inode(inode_num, inode);

    uint32_t file_size = inode_get_size(inode);
    if(pos > file_size)
        return FAIL;

    handle_set_position(fd, (uint32_t)pos);

    return OK;
}


/**
 * Vrati aktualnu poziciu v subore.
 */

size_t fs_tell(file_t *fd) {
    return handle_get_position(fd);
}


/**
 * Vrati informacie o 'path'.
 *
 * Funkcia vrati FAIL ak cesta neexistuje, alebo vyplni v strukture 'fs_stat'
 * polozky a vrati OK:
 *  - st_size: velkost suboru v byte-och
 *  - st_nlink: pocet hardlinkov na subor (ak neimplementujete hardlinky, tak 1)
 *  - st_type: hodnota podla makier v hlavickovom subore: STAT_TYPE_FILE,
 *  STAT_TYPE_DIR, STAT_TYPE_SYMLINK
 *
 */

int fs_stat(const char *path, struct fs_stat *fs_stat) {
    char* path_copy = strdup(path);
    int32_t inode_num = get_inode_num(path_copy);
    if(inode_num == FAIL)
        return FAIL; // Taky inode neexistuje, teda subor neexistuje

    uint8_t inode[SECTOR_SIZE];
    read_inode((uint32_t)inode_num, inode);

    fs_stat->st_size = inode_get_size(inode);
    fs_stat->st_nlink = inode_get_num_hardlinks(inode);
    uint32_t type = inode_get_type(inode);
    fs_stat->st_type = type;

    free(path_copy);

    return OK;
}

/* Level 3 */
/**
 * Vytvori adresar 'path'.
 *
 * Ak cesta, v ktorej adresar ma byt, neexistuje, vrati FAIL (vytvara najviac
 * jeden adresar), pri korektnom vytvoreni OK.
 */
int fs_mkdir(const char *path)
{
    char* path_copy = strdup(path);
    int32_t old_inode_num = get_inode_num(path_copy);
    if(old_inode_num != FAIL) {
        return FAIL; // Uz existuje
    }

    uint32_t dirname_len = get_filename_len_from_path(path);
    char* dirname = calloc(dirname_len + 1, sizeof(char));

    uint32_t parent_path_len = (uint32_t)strlen(path) - dirname_len;
    // Ak je parent iba /, tak nechceme prazdny strin, ale "/". Je to jediny pripad kedy nechceme davat prec / z konca
    char* parent_path = calloc(parent_path_len + 1 + (parent_path_len == 0 ? 1 : 0), sizeof(char));

    // Bez / na konci (okrem samotneho "/")
    for(uint32_t i = 0; i < parent_path_len - 1; i++) {
        parent_path[i] = path[i];
    }
    if(parent_path_len == 1)
        parent_path[0] = '/'; // Uz vyssie sme to spravili tak, ze tu bude miesto na ten jeden znak

    // Preskocime / medzi parent_path a dir_name
    for(uint32_t i = parent_path_len; i < parent_path_len + dirname_len; i++) {
        dirname[i - parent_path_len] = path[i];
    }

    // Najdeme inode parenta a zistime, ci existuje
    int32_t parent_inode_num = get_inode_num(parent_path);
    if(parent_inode_num == -1) {
        return FAIL;
    }

    uint8_t parent_inode[SECTOR_SIZE];
    read_inode((uint32_t)parent_inode_num, parent_inode);
    uint32_t parent_type = inode_get_type(parent_inode);
    if(parent_type != STAT_TYPE_DIR) {
        return FAIL; // Parent nie je directory, takze cesta nemoze pokracovat
    }

    int32_t new_inode_num = get_free_inode_num();
    setup_new_inode((uint32_t)new_inode_num, STAT_TYPE_DIR);
    if(new_inode_num == NO_FREE_INODE) {
        return FAIL; // Uz nemame miesto na novy directory
    }
    add_to_directory((uint32_t)parent_inode_num, dirname, (uint32_t)new_inode_num);

    free(path_copy);
    free(dirname);
    free(parent_path);

    return OK;
}

/**
 * Odstrani adresar 'path'.
 *
 * Odstrani adresar, na ktory ukazuje 'path'; ak neexistuje alebo nie je
 * adresar, vrati FAIL; po uspesnom dokonceni vrati OK.
 */
int fs_rmdir(const char *path)
{
    char* path_copy = strdup(path);

    int32_t inode_num = get_inode_num(path_copy);

    if(inode_num == -1) {
        return FAIL;
    }

    uint8_t inode[SECTOR_SIZE];
    read_inode((uint32_t)inode_num, inode);

    int32_t type = inode_get_type(inode);
    if(type != STAT_TYPE_DIR) {
        return FAIL; // Nie je to adresar
    }

    uint32_t size = inode_get_size(inode);
    if(size != 0) {
        return FAIL; // Je neprazdny, nemozeme ho zmazat
    }

    uint32_t dirname_len = get_filename_len_from_path(path);
    char* dirname = calloc(dirname_len + 1, sizeof(char));

    uint32_t parent_path_len = (uint32_t)strlen(path) - dirname_len;
    // Ak je parent iba /, tak nechceme prazdny strin, ale "/". Je to jediny pripad kedy nechceme davat prec / z konca
    char* parent_path = calloc(parent_path_len + 1 + (parent_path_len == 0 ? 1 : 0), sizeof(char));

    // Bez / na konci (okrem samotneho "/")
    for(uint32_t i = 0; i < parent_path_len - 1; i++) {
        parent_path[i] = path[i];
    }
    if(parent_path_len == 1)
        parent_path[0] = '/'; // Uz vyssie sme to spravili tak, ze tu bude miesto na ten jeden znak

    // Preskocime / medzi parent_path a dir_name
    for(uint32_t i = parent_path_len; i < parent_path_len + dirname_len; i++) {
        dirname[i - parent_path_len] = path[i];
    }

    int32_t parent_inode_num = get_inode_num(parent_path); // Existuje, lebo aj path existuje
    remove_from_dir((uint32_t)parent_inode_num, dirname, (uint32_t)inode_num, 2); // 2 = uvolni inode, nie bloky (lebo prazdny ani nema bloky)

    free(path_copy);
    free(dirname);
    free(parent_path);

    return OK;
}

/**
 * Otvori adresar 'path' (na citanie poloziek)
 *
 * Vrati handle na otvoreny adresar s poziciou nastavenou na 0; alebo FAIL v
 * pripade zlyhania.
 */
file_t *fs_opendir(const char *path)
{
    char* path_copy = strdup(path);
    int32_t inode_num = get_inode_num(path_copy);

    if(inode_num == FAIL) {
        return NULL; // Taky directory neexistuje
    }

    uint8_t inode[SECTOR_SIZE];
    read_inode((uint32_t)inode_num, inode);

    uint32_t type = inode_get_type(inode);
    if(type != STAT_TYPE_DIR) {
        return NULL; // Nie je to directory
    }

    file_t* fd = fd_alloc();
    handle_set_position(fd, 0);
    handle_set_inode(fd, (uint32_t)inode_num);
    uint32_t first_item_sector = inode_dir_get_first_item_sector(inode);
    uint8_t first_item_slot = inode_dir_get_first_item_slot(inode);
    handle_set_curr_item_sector(fd, first_item_sector);
    handle_set_curr_item_slot(fd, first_item_slot);

    free(path_copy);

    return fd;
}

/**
 * Nacita nazov dalsej polozky z adresara.
 *
 * Do dodaneho buffera ulozi nazov polozky v adresari, posunie aktualnu
 * poziciu na dalsiu polozku a vrati OK.
 * V pripade problemu, alebo ak nasledujuca polozka neexistuje, vracia FAIL.
 * (V pripade jedneho suboru v adresari vracia FAIL az pri druhom volani.)
 */
int fs_readdir(file_t* dir, char* item)
{
    uint32_t curr_item_sector = handle_get_curr_item_sector(dir);
    uint8_t curr_item_slot = handle_get_curr_item_slot(dir);

    if(curr_item_sector == NOTHING) {
        return FAIL; // Uz nie su dalsie polozky
    }

    uint8_t sector[SECTOR_SIZE];
    hdd_read(curr_item_sector, sector);

    char full_name[MAX_FILENAME];
    dir_entry_get_name(curr_item_sector, curr_item_slot, full_name);

    // Z nejakeho dovodu som v directory zarovnaval meno do MAX_FILENAME bajtov nulovymi bajtami zlava,
    // co je hlupe riesenie a mal som ho samozrejme zarovnavat sprava
    uint32_t name_len = MAX_FILENAME;
    for(int i = 0; i < MAX_FILENAME; i++) {
        if(full_name[i] == 0)
            name_len--;
        else
            break;
    }
    for(int i = MAX_FILENAME - name_len, j = 0; i < MAX_FILENAME; i++, j++) {
        item[j] = full_name[i];
    }
    item[name_len] = '\0';

    // Posunieme sa na dalsiu polozku
    uint32_t nxt_item_sector = dir_get_nxt_item_sector(sector, curr_item_slot);
    uint8_t nxt_item_slot = dir_get_nxt_item_slot(sector, curr_item_slot);

    handle_set_curr_item_sector(dir, nxt_item_sector);
    handle_set_curr_item_slot(dir, nxt_item_slot);

    return OK;
}

/** 
 * Zatvori otvoreny adresar.
 */
int fs_closedir(file_t* dir)
{
    fd_free(dir);
    return OK;
}

/* Level 4 */
/**
 * Vytvori hardlink zo suboru 'path' na 'linkpath'.
 */
int fs_link(const char *path, const char *linkpath) {
    char* path_copy = strdup(path);
    int32_t inode_num = get_inode_num(path_copy);
    if(inode_num == FAIL) {
        return FAIL; // Neexistuje subor na path
    }

    uint8_t inode[SECTOR_SIZE];
    read_inode((uint32_t)inode_num, inode);

    uint32_t type = inode_get_type(inode);
    if(type != STAT_TYPE_FILE) {
        return FAIL; // Mozeme robit hardlinky iba na obycajny file
    }

    char* linkpath_copy = strdup(linkpath);
    uint32_t linkname_len = get_filename_len_from_path(linkpath_copy);
    uint32_t link_parent_path_len = (uint32_t)strlen(linkpath) - linkname_len;

    char* linkname = calloc(linkname_len + 1, sizeof(char)); // +1 na \0
    for(uint32_t i = (uint32_t)strlen(linkpath) - 1; i >= link_parent_path_len; i--) {
        linkname[i - link_parent_path_len] = linkpath[i];
    }

    char* link_parent_path = calloc(link_parent_path_len + 1, sizeof(char)); // +1 na \0
    for(uint32_t i = 0; i < link_parent_path_len; i++)
        link_parent_path[i] = linkpath[i];

    int32_t link_parent_inode_num = get_inode_num(link_parent_path);
    if(link_parent_inode_num == FAIL) {
        return FAIL; // Directory v ktorom ma byt link neexistuje
    }

    int32_t add_result = add_to_directory((uint32_t)link_parent_inode_num, linkname, (uint32_t)inode_num);
    if(add_result == FAIL) {
        return FAIL; // Nepodarilo sa pridat link do toho directory kde chce byt
    }

    uint32_t num_hard_links = inode_get_num_hardlinks(inode);
    assert(num_hard_links < (1 << 16));
    inode_set_num_hard_links(inode, (uint16_t)(num_hard_links + 1)); // Zvysime pocet hardlinkov o 1
    write_inode((uint32_t)inode_num, inode);

    free(path_copy);
    free(linkpath_copy);
    free(linkname);
    free(link_parent_path);

    return OK;
}

/**
 * Vytvori symlink z 'path' na 'linkpath'.
 */
int fs_symlink(const char *path, const char *linkpath) {
    char* path_copy = strdup(path);
    int32_t inode_num = get_inode_num(path_copy);

    if(inode_num == FAIL) {
        return FAIL; // Neexistuje subor na path
    }

    char* linkpath_copy = strdup(linkpath);
    uint32_t linkname_len = get_filename_len_from_path(linkpath_copy);
    uint32_t link_parent_path_len = (uint32_t)strlen(linkpath) - linkname_len;

    char* linkname = calloc(linkname_len + 1, sizeof(char));
    for(uint32_t i = (uint32_t)strlen(linkpath) - 1; i >= link_parent_path_len; i--) {
        linkname[i - link_parent_path_len] = linkpath[i];
    }

    char* link_parent_path = calloc(link_parent_path_len + 1, sizeof(char));
    for(uint32_t i = 0; i < link_parent_path_len; i++)
        link_parent_path[i] = linkpath[i];

    int32_t link_parent_inode_num = get_inode_num(link_parent_path);
    if(link_parent_inode_num == FAIL) {
        return FAIL; // Neexistuje directory v ktorom chceme vytvorit symlink
    }

    int32_t symlink_inode_num = get_free_inode_num();
    if(symlink_inode_num == NO_FREE_INODE) {
        return FAIL; // Uz nemame volny inode pre tento symlink
    }
    mark_inode((uint32_t)symlink_inode_num, USED);

    uint8_t symlink_inode[SECTOR_SIZE] = {0};
    inode_set_type(symlink_inode, STAT_TYPE_SYMLINK);
    inode_set_num_hard_links(symlink_inode, 1);
    inode_set_size(symlink_inode, (uint32_t)strlen(linkname));

    for(uint32_t i = 6; i < SECTOR_SIZE; i++) {
        symlink_inode[i] = 0; // Znulujeme si priestor pre symlinkovu path v jeho inode
    }
    for(uint32_t i = 6; i < 6 + (uint32_t)strlen(path); i++) {
        symlink_inode[i] = (uint8_t)path[i - 6];
    }

    write_inode((uint32_t)symlink_inode_num, symlink_inode);

    add_to_directory((uint32_t)link_parent_inode_num, linkname, (uint32_t)symlink_inode_num);

    free(path_copy);
    free(linkpath_copy);
    free(linkname);
    free(link_parent_path);

    return OK;
}
